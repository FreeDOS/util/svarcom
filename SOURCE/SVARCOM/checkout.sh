#!/bin/bash

# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2021.0
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2022.0
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2022.1
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2022.2
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2022.3
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2022.4
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2024.1
# svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2024.2
svn co svn://svn.svardos.org/svardos/svarcom/tags/svarcom-2024.3

rm -rf */.svn

# svn co svn://svn.svardos.org/svardos/svarcom/trunk latest
